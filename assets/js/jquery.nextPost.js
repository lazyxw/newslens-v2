
(function($)
{   

    var NextPost=function(options)
    {

        var $this=this;

        $this.defaults=
        {

        };

        $this.options=$.extend($this.defaults,options);

        $this.nextPost=$('#next-post');
        $this.prevPost=$('#prev-post');
        
        $this.nextPostClose=false;
        $this.prevPostClose=false;
        
        $this.state={'next':0,'prev':0};

        this.load=function()
        {
            $this.setStyle($this.nextPost,$this.options.nextPost,'next');
            $this.setStyle($this.prevPost,$this.options.prevPost,'prev');

            $(window).bind('scroll',function() 
            {
                $this.process($this.nextPost,$this.options.nextPost,'next');
                $this.process($this.prevPost,$this.options.prevPost,'prev');
            });
            
            $(window).bind('resize',function() 
            {
                $this.process($this.nextPost,$this.options.nextPost,'next');
                $this.process($this.prevPost,$this.options.prevPost,'prev');
            });
            
            $this.nextPost.find('a.next-post-close-button').bind('click',function(e) 
            {
                e.preventDefault();
                
                $this.nextPostClose=true;
                $this.close($this.nextPost,$this.options.nextPost,'next');
            });
            
            $this.prevPost.find('a.next-post-close-button').bind('click',function(e) 
            {
                e.preventDefault();
                
                $this.prevPostClose=true;
                $this.close($this.prevPost,$this.options.prevPost,'prev');
            });     
            
            $(window).trigger('scroll');
        };
        
        this.getWindowWidth=function()
        {
            return($(window).width());
        };

        this.getWindowHeight=function()
        {
            return($(window).height());
        };

        this.getScrollPercent=function()
        {
            var windowHeight=parseInt($(window).height());
            var documentHeight=parseInt($(document).height());
            var scrollPosition=parseInt($(window).scrollTop());

            var result=Math.round((scrollPosition/(documentHeight-windowHeight))*100);

            return(result);
        };
        
        this.isScrolledIntoView=function(elem)
        {
            var docViewTop=$(window).scrollTop();
            var docViewBottom=docViewTop+$(window).height();

            var elemTop=$(elem).offset().top;

            return(docViewBottom>=elemTop);
        };

        this.process=function(object,options,type)
        {
            if(object.length!=1) return;
            if((type=='next') && ($this.nextPostClose)) return;
            if((type=='prev') && ($this.prevPostClose)) return;

            var windowWidth=$this.getWindowWidth();
            var windowHeight=$this.getWindowHeight()

            if(options.responsive.width.enable==1)
            {
                if(windowWidth<=options.responsive.width.value)
                {
                    $this.close(object,options,type);
                    return;
                }   
            }
            
            if(options.responsive.height.enable==1)
            {
                if(windowHeight<=options.responsive.height.value)
                {
                    $this.close(object,options,type);
                    return;
                }   
            }
            
            var width=$this.getBoxWidth(options);
            object.css('width',width+'px');

            var elementSelector=jQuery.trim(options.dimension.offsetElement);
            
            if($(elementSelector).first().length==1)
            {
                var element=$(elementSelector).first();
                var visible=$this.isScrolledIntoView(element);
            
                if(visible) $this.open(object,options,type);
                else $this.close(object,options,type);
            }
            else
            {
                var percent=$this.getScrollPercent();
                if(percent>=options.dimension.offset) $this.open(object,options,type);
                else $this.close(object,options,type);
            }
        };
        
        this.open=function(object,options,type)
        {
            if(($this.state[type]==2) || ($this.state[type]==3)) return;
            
            $this.state[type]=2;
            
            var style=$this.getAnimationStyle(object,options,type,'open');
            object.css({display:'block'});
            
            object.stop(false,false).animate(style.to,{easing:options.animation.open.easing,duration:options.animation.open.speed,complete:function() 
            {
                $this.state[type]=3;
                $(this).addClass('next-post-open');
            }});        
        };
        
        this.close=function(object,options,type)
        {
            if(($this.state[type]==0) || ($this.state[type]==1)) return;
            
            $this.state[type]=1;
            
            var style=$this.getAnimationStyle(object,options,type,'close');

            object.stop(false,false).animate(style.to,{easing:options.animation.close.easing,duration:options.animation.close.speed,complete:function() 
            {
                $this.state[type]=0;
                object.css({display:'none'});
                $(this).removeClass('next-post-open');
            }});        
        };

        this.getAnimationStyle=function(object,options,type,action)
        {
            var style={};

            switch(options.animation.type)
            {
                case 'slide':

                    switch(type)
                    {
                        case 'next':

                            if(action=='open')  style={to:{right:'0px'}};
                            if(action=='close') style={to:{right:(-1*object.outerWidth(true))+'px'}};

                        break;

                        case 'prev':
                            
                            if(action=='open')  style={to:{left:'0px'}};
                            if(action=='close') style={to:{left:(-1*object.outerWidth(true))+'px'}};

                        break;
                    }

                break;

                case 'fade':

                    if(action=='open')  style={to:{opacity:1}};
                    if(action=='close') style={to:{opacity:0}};
                    
                break;
            }

            return(style);
        };

        this.setStyle=function(object,options,type)
        {
            if(object.length!=1) return;
            
            object.css('width',$this.getBoxWidth(options)+'px');
            object.css(options.dimension.position.type,options.dimension.position.value+'%');

            switch(options.animation.type)
            {
                case 'slide':

                    if(type=='next') object.css({right:(-1*object.outerWidth(true))+'px'});
                    if(type=='prev') object.css({left:(-1*object.outerWidth(true))+'px'});

                break;

                case 'fade':

                    object.css({opacity:0});

                break;
            }
        };
        
        this.getBoxWidth=function(options)
        {
            var width=options.dimension.box.width;
            
            if($this.options.general.responsive.enable==1)
            {
                var windowWidth=$this.getWindowWidth();
                if(width>windowWidth) return(windowWidth);
            }
            
            return(width);
        }

    };

    $.fn.nextPost=function(options)
    {
        var nextPost=new NextPost(options);
        nextPost.load();

    };


})(jQuery);