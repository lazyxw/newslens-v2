<?php

if( is_single() ){
// flyout post nav
?>
<script type="text/javascript">

  jQuery(document).ready(function($)
  {
    $(window).load(function() 
    {
      $().nextPost({
        "nextPost":{"dimension":{"offset":0,"offsetElement":"#dd_end","position":{"type":"bottom","value":10},"box":{"width":350}},"animation":{"type":"slide","open":{"easing":"easeInOutSine","speed":300},"close":{"easing":"easeInExpo","speed":200}},"responsive":{"width":{"enable":1,"value":800},"height":{"enable":0,"value":0}}},
        "prevPost":{"dimension":{"offset":60,"offsetElement":"","position":{"type":"bottom","value":0},"box":{"width":350}},"animation":{"type":"slide","open":{"easing":"easeInOutSine","speed":300},"close":{"easing":"easeInExpo","speed":200}},"responsive":{"width":{"enable":1,"value":800},"height":{"enable":0,"value":0}}},"general":{"responsive":{"enable":1}}
      });
    });
  });

</script>

<?php

  $prev_post = get_previous_post();
  $next_post = get_next_post();
  $post_nav_html = '';
  $tracking = '?ref=fo';

  if ( !empty($next_post) ){
    $np_id = $next_post->ID;
    $np_link = get_permalink($np_id);
    $np_title = $next_post->post_title;
    $np_desc = summarize_long_text($next_post->post_content, 80) . '...';

    $obj_img = wp_get_attachment_image_src( get_post_thumbnail_id($np_id), 'art-thumb');
    $np_img = $obj_img[0];
    $post_nav_html .= '
    <div class="clear-fix">
      <h3 class="next-post-header"> &raquo; 下一篇文章</h3>
      <a href="' . $np_link . $tracking . '" title="' . $np_title . '" class="next-post-image"><img width="64" height="64" src="' .  $np_img . '" class="wp-post-image" alt="2" /></a>
      <a href="' . $np_link . $tracking . '" title="' . $np_title . '" class="next-post-title">' . $np_title . '</a>
    </div>
    ';
  }

  if ( !empty($prev_post) ){
    $pp_id = $prev_post->ID;
    $pp_link = get_permalink($pp_id);
    $pp_title = $prev_post->post_title;
    $pp_desc = summarize_long_text($prev_post->post_content, 80) . '...';

    $obj_img = wp_get_attachment_image_src( get_post_thumbnail_id($pp_id), 'art-thumb');
    $pp_img = $obj_img[0];
    $post_nav_html .= '
    <div class="clear-fix">
      <h3 class="next-post-header"> &laquo; 上一篇文章</h3>
      <a href="' . $pp_link . $tracking . '" title="' . $pp_title . '" class="next-post-image"><img width="64" height="64" src="' .  $pp_img . '" class="wp-post-image" alt="2" /></a>
      <a href="' . $pp_link . $tracking . '" title="' . $pp_title . '" class="next-post-title">' . $pp_title . '</a>
    </div>
    ';
  }

  if ( ! empty($post_nav_html) ) {
?>
<div id="next-post" class="next-post">
    <div class="clear-fix"><a href="#" class="next-post-close-button"></a></div>
    <?php echo $post_nav_html; ?>
</div>
<?php
  }

}
