<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="panel">
        <div class="panel-heading">
            <div class="post-tags"> 
                <span style="color:#a3a3a3;">文章標籤：</span>
                <span style="display: block;">
                <?php
                   $posttags = get_the_tags();
                    if ($posttags) 
                    {
                        foreach ($posttags as $tag) 
                        {
                ?>
                    <span class="label label-info" style="color:#fff; margin-right:2px;"><a class="post-cat-box" style="color:#fff;" href="<?php echo get_tag_link($tag->term_id); ?>">
                        <?php echo $tag->name; ?>
                    </a></span>
                <?php
                        }
                    }
                ?>           
                </span>
            </div>
        </div>

        <div class="media">
            <div class="media-heading text-center">
                <h2 class="media-heading entry-title" style="padding:10px; font-color:#c8c8c8;"><b><?php the_title(); ?></b></h2>
                <?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?>
                <div class="entry-info" style="color: #a3a3a3;">
                    <span id="author-name"><?php coauthors_posts_links(' ', ' '); ?> </span><span id="date"><time class="entry-date" datetime="<?php echo date(DATE_W3C); ?>" ><?php the_time(get_option('date_format')) ?></time></span> 發表於

                    <?php
                    $categories = get_the_category();
                    $cate_container = array();

                    if($categories){
                        foreach($categories as $category) {
                            if ($category->slug != 'featured') { //ignore the featured category name
                                $cate_name = $category->name;

                                if( $category->slug == 'news') $cate_name = str_replace('*', '', $cate_name);

                                $parent = get_category($category->category_parent);

                    ?>
                                <span><a class="post-cat-box" href="<?php echo get_category_link($category->cat_ID); ?>">&#149;&#32;<?php echo $cate_name; ?></a></span>
                    <?php
                                $cate_container[] = $category->name;
                            }
                        }
                    }
                    ?>
                </div>
            </div>

            <div style="width:290px; margin:20px auto 0 auto;">
                <?php require('inc/share.php'); ?>
            </div>
            <?php 
                $en_content = get_post_meta( get_the_ID(), 'en_content', true );

                if(!empty($en_content)){
            ?>
            <div style="padding-top: 29px;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#zh-content" data-toggle="tab">中文</a></li>
                    <li><a href="#en-content" data-toggle="tab">English</a></li>
                </ul>
            </div>
            <?php
                }
            ?>

            <div class="clearfix"></div>



            <div id="entry-content" class="media-body entry-content tab-content" style="font-size:17px; padding:10px">
                <div class="tab-pane fade active in" id="zh-content">
                    <?php
                        $content = get_the_content();
                        $content = '<a id="dd_start"></a>' . $content . "\n<a id=\"dd_end\"></a>";

                        preg_match('/\[caption(.*?)\[\/caption\]/i', $content, $match);

                        if( isset($match[0]) ){

                            $content = str_replace($match[0], '', $content);
                            $content .= $match[0];
                            $img_html = $match[0];

                            preg_match('/<img [^>]*src=["|\']([^"|\']+)/i', $img_html, $img_match);
                            if(isset($img_match[1])){
                                $bg_src = $img_match[1];
                                $bg_src = apply_filters( 'jetpack_photon_url', $bg_src );
                                define('STORY_BG', $bg_src);
                            }
                        }


                        $content = apply_filters('the_content', $content);
                        $content = str_replace(']]>', ']]&gt;', $content);
                        $content_container = explode("</p>", $content);

                        $param_count = count($content_container);

                        foreach ($content_container as $key => $value) {
                            if( !in_array('影片', $cate_container) && is_singular() ){
                                if( $param_count > 7 and 5 == $key )
                                    require('inc/SF_STORY_CONTENT-INSIDE_300x250.php');
                                else if( $param_count > 15 and 13 == $key )
                                    require('inc/SF_STORY_CONTENT-INSIDE2_300x250.php');
                            }



                            echo $value . '</p>';
                        }

                        
                        require('inc/SF_STORY_BELOW-CONTENT_728x90.php');
                        require('inc/related_story.php');
                    ?>
                </div>
                <?php if(!empty($en_content)): ?>
                <div class="tab-pane fade" id="en-content">
                    <?php
                    $en_content = apply_filters('the_content', $en_content);
                    $en_content = str_replace(']]>', ']]&gt;', $en_content);
                        
                    echo $en_content; 
                    ?>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
<?php require('plugin/floating-share.php'); ?>
</article><!-- #post -->
    <div class="well hidden-sm">
         <div class="photo sitemajiad" model="3"></div>
    </div>

    <div class="panel visible-sm">
        <?php get_template_part( 'author-bio' ); ?>
    </div>



    <a name="comment-panel"></a>
    <div class="panel">
    <?php comments_template(); ?>
    </div>