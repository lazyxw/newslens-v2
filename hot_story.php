<?php
/*
    Template Name: hot story list
*/


$args = array(
    'type'                     => 'post',
    'child_of'                 => 0,
    'parent'                   => '',
    'orderby'                  => 'name',
    'order'                    => 'ASC',
    'hide_empty'               => 1,
    'hierarchical'             => 1,
    'exclude'                  => '1,19',
    'include'                  => '',
    'number'                   => '',
    'taxonomy'                 => 'category',
    'pad_counts'               => false 

);

$cate_list = get_categories( $args );
// print_r($cate_list);

$args_posts = array(
    'posts_per_page'   => 10,
    'offset'           => 0,
    'category'         => '',
    'orderby'          => 'meta_value_num',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => 'post_views_count',
    'meta_value'       => '',
    'post_type'        => 'post',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'post_status'      => 'publish',
    'suppress_filters' => true );

foreach($cate_list as $cate){
    echo "<p><h1>{$cate->name} ({$cate->category_count})</h1>";

    $args_posts['category'] = $cate->cat_ID;
    $post_list = get_posts($args_posts);

    // print_r($post_list);
    foreach($post_list as $post){
        $ID = $post->ID;
        $url = get_permalink( $ID );
        $post_view = get_post_meta($ID, 'post_views_count', true);

        echo "<h3><a href=\"{$url}\" target=\"_blank\">{$post->post_title}</a> ({$post_view})</h3>";


    }
    echo '</p>';
}

$posts_array = get_posts( $args );