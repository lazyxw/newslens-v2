<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41537444-1', 'thenewslens.com');

<?php
if(is_singular()){

  // track category & author in singular
  $categories = get_the_category();
  if($categories){
    $c_count = 1;
    foreach($categories as $category) {
      if ($category->name != TD_FEATURED_CAT) {
        $c_count++;
        if($c_count > 5) break;
?>
    ga('set', 'dimension<?=$c_count?>', '<?=$category->name?>');
    ga('send', 'event', 'visit', 'category', '<?=$category->name?>', 1, {'nonInteraction': true});
<?php
      }
    }
  }

  the_post();
?>
    ga('set', 'dimension1', '<?=get_the_author()?>');
    ga('send', 'event', 'visit', 'author', '<?=get_the_author()?>', 1, {'nonInteraction': true});
<?php
  rewind_posts();

} else if(is_category()) {

  // track category in category archive
  $curCategory = get_query_var('cat');
  $yourcat = get_category($curCategory);
?>
    ga('set', 'dimension2', '<?=$yourcat->name?>');
    ga('send', 'event', 'visit', 'category_home', '<?=$yourcat->name?>', 1, {'nonInteraction': true});
<?php

} else if(is_author()) {

  // track author in author archive
?>
    ga('set', 'dimension1', '<?=get_the_author()?>');
    ga('send', 'event', 'visit', 'author', '<?=get_the_author()?>', 1, {'nonInteraction': true});
<?php

}

if (is_user_logged_in()){
  global $current_user;
  $user_roles = implode(',', $current_user->roles);
?>
    ga('set', 'dimension5', '<?php echo $current_user->user_nicename; ?>');
    ga('set', 'dimension6', '<?php echo $user_roles; ?>');
<?php
}


?>
<?php if(is_404()):?>
    ga('send', 'event', 'Error', '404', 'page: ' + document.location.pathname + document.location.search + ' ref: ' + document.referrer ], 1, {'nonInteraction': true});
<?php endif; ?>

    ga('send', 'pageview');

</script>