<?php

// custom exercpt function
function summarize_long_text($str, $length = 60){
    $str = strip_tags($str);
    $str = str_replace("\n", ' ', $str);
    $str = preg_replace('/\[caption(.*?)\[\/caption\]/i', '', $str);

    if( $length == -1 ){
        $desc = $str;
    } else {
        $desc = mb_substr($str, 0, $length, 'UTF8');
    }

    // preg_match('/[\x{4e00}-\x{9fa5}]+.*/u', $str, $match);
    // $desc = mb_substr($match[0], 0, $length, 'UTF8');

    return $desc;
}

// pagination
function wpbeginner_numeric_posts_nav($query, $forced = false) {

    if( is_singular() and ! $forced )
        return;

    $wp_query = $query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 2 ) {
        $links[] = $paged - 1;
        // $links[] = $paged - 2;
    }

    if ( ( $paged + 1 ) <= $max ) {
        // $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul class="pagination">' . "\n";

    /** Previous Post Link */

    if ( $paged > 1 ){
        printf( '<li id="previous_page"%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $paged - 1 ) ), '&laquo;' );
    } else {
        echo '<li class="disabled"><a href="javascript:;">&laquo;</a></li>';
    }

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li id="previous_page"%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li id="next_page" id="previous_page"><a href="javascript:;">...</a></li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li><a href="javascript:;">...</a></li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( $paged < $max ){
        printf( '<li id="previous_page"%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $paged + 1 ) ), '&raquo;' );
    }else{
        echo '<li id="next_page" class="disabled"><a href="javascript:;">&raquo;</a></li>';
    }

    echo '</ul>' . "\n";

}

// get avatar url
// function get_avatar_url($get_avatar){

//     preg_match("/src=['|\"](.*?)['|\"]/i", $get_avatar, $matches);
//     //print_r($matches);
//     return $matches[1];
// }

function author_add_social($authorMetaKey) {
    $authorMeta = get_the_author_meta($authorMetaKey);
    if (!empty($authorMeta)) {
        echo '<span class="td-social-wrap"><a href="' . $authorMeta . '" target="_blank"><img class="td-retina td-social-icon td-social-' . $authorMetaKey . ' td-style-1" src="' . get_template_directory_uri() . '/assets/img/icons/' . $authorMetaKey . '_square.png' .'" width="32" height="32" alt=""/></a></span>';
    }
}

function menu_user_icon()
    {
    if (is_user_logged_in())
    {
        global $current_user; get_currentuserinfo();

        $avatar_url = get_avatar_url(str_replace("'", '"', get_avatar( $current_user->ID, 28)));

        return '<img src ="' . $avatar_url . '" class = "img-rounded" style = "width : 28px;">';

    }
    else
    {
        return '<i class="glyphicon glyphicon-user pull-right" style="margin-top:5px;"></i>';
    }

}

function menu_user_email()
    {
        if (is_user_logged_in())
        {
            global $current_user; get_currentuserinfo();

            $user_email_url = $current_user->user_email;
            echo $user_email_url;
            return NULL;

        }
        else
        {
            return NULL;
        }

    }