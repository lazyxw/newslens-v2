<?php
if( is_singular() ){
    $DISABLE_AD_LIST = array(8979);
    if ( in_array(get_the_ID(), $DISABLE_AD_LIST)){
        define('DISABLE_ADSENSE', true);
    }
}

?>
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
googletag.cmd.push(function() {

<?php if( is_home() ): ?>

googletag.defineSlot('/112152674/TNL_HOME_LATEST-1ST_300x250', [300, 250], 'div-gpt-ad-1383726660926-3').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_HOME_LATEST-BOTTOM_300x250', [300, 250], 'div-gpt-ad-1383726660926-4').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_HOME_POPULAR-1ST_300x250', [300, 250], 'div-gpt-ad-1383726660926-5').addService(googletag.pubads());

<?php elseif( is_category() or is_tag() ): ?>

googletag.defineSlot('/112152674/TNL_CHANNEL_LATEST-1ST_300x250', [300, 250], 'div-gpt-ad-1383726660926-0').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_CHANNEL_LATEST-BOTTOM_300x250', [300, 250], 'div-gpt-ad-1383726660926-1').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_CHANNEL_POPULAR-1ST_300x250', [300, 250], 'div-gpt-ad-1383726660926-2').addService(googletag.pubads());

<?php elseif(is_author()): ?>

googletag.defineSlot('/112152674/TNL_CHANNEL_LATEST-1ST_300x250', [300, 250], 'div-gpt-ad-1383726660926-0').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_CHANNEL_LATEST-BOTTOM_300x250', [300, 250], 'div-gpt-ad-1383726660926-1').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_AuthorPage_160x600', [300, 600], 'div-gpt-ad-1380191096435-11').addService(googletag.pubads());

<?php elseif(is_singular()): ?>

googletag.defineSlot('/112152674/TNL_STORY_BELOW-CONTENT_728x90', [728, 90], 'div-gpt-ad-1383726660926-6').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_STORY_BELOW-TITLE_728x90', [728, 90], 'div-gpt-ad-1383726660926-7').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_STORY_CONTENT-INSIDE_300x250', [300, 250], 'div-gpt-ad-1383726660926-8').addService(googletag.pubads());
googletag.defineSlot('/112152674/TNL_STORY_SIDEBAR-HEAD_300x250', [300, 250], 'div-gpt-ad-1383726660926-9').addService(googletag.pubads());
<?php /* googletag.defineSlot('/112152674/TNL_STORY_SIDEBAR-MIDDLE_300x600', [300, 600], 'div-gpt-ad-1383726660926-10').addService(googletag.pubads()); */ ?>
googletag.defineSlot('/7682122/thenewslens_article_160x600_RB', [160, 600], 'div-gpt-ad-1384231424828-0').addService(googletag.pubads());


<?php endif; ?>

<?php
// set target tag
if(is_singular()){

    $categories = get_the_category();
    if($categories){
        $cate = array();
        foreach($categories as $category) {
            $cate[] = $category->slug;
        }

        if( count( $cate ) > 0 ){
            $cate_str = implode('","', $cate);
?>
googletag.pubads().setTargeting("category",["<?php echo $cate_str; ?>"]);
<?php
        }
    }

    $posttags = get_the_tags();
    if ($posttags) {
        $tags = array();
        foreach ($posttags as $tag) {
            $tags[] = $tag->name;
        }
        if( count( $tags ) > 0 ){
            $tag_str = implode('","', $tags);
?>
googletag.pubads().setTargeting("tag",["<?php echo $tag_str; ?>"]);
<?php
        }
    }

    the_post();
?>
googletag.pubads().setTargeting("author","<?php echo get_the_author(); ?>");
<?php
    rewind_posts();

} else if(is_category()) {

    // track category in category archive
    $curCategory = get_query_var('cat');
    $yourcat = get_category($curCategory);
?>
googletag.pubads().setTargeting("category","<?php echo $yourcat->slug; ?>");
<?php

} else if(is_author()) {

  // track author in author archive
?>
googletag.pubads().setTargeting("author","<?php echo get_the_author(); ?>");
<?php

}

?>
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
