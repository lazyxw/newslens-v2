<?php if( is_singular() ): ?>
<!-- OneAD® InPage 開始 -->
<script type="text/javascript"> 
    var ONEAD = {};
    ONEAD.channel = 21; // 關鍵評論網：內文版位
    ONEAD.volume =  0.2; // range is 0 to 1 (float) 
    ONEAD.slot_limit = {width: 990, height: 406};
    // optional(s)
    //ONEAD.response_freq = [1, 4, 7, 10, 13, 16, 19, 22];
    ONEAD.response_freq = {start:1, step: 7};
    // async 
    ONEAD.cmd = ONEAD.cmd || [];
</script>
<script type="text/javascript">
// For OneAD, DON'T MODIFY the following
if (ONEAD){
    ONEAD.uid = "1000046";  
    ONEAD.external_url = "http://onead.onevision.com.tw/"; // base_url, post-slash is necessary
    ONEAD.wrapper = 'ONEAD_player_wrapper'; 
}

if (typeof window.isip_js == "undefined") { 
    // document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');

    // async
    (function() {
    var src = 'http://ad-specs.guoshipartners.com/static/js/isip.js';   
    var js = document.createElement('script');
    js.async = true;
    js.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    js.src = src;   
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(js, node.nextSibling); // insert after
    })();   
}
</script>

<script type="text/javascript">
    // ONEAD, this is tempate if you want to overwrite
    /*
    ONEAD_get_response = function(param){
        if (param.embed_string){
            // show ads
        }   
    }
    */
    function removeElement(element) {
        element && element.parentNode && element.parentNode.removeChild(element);
    }
    // 這個函式名稱是固定的，廣告播放完畢會呼叫之
    function changeADState(obj){            
        // following is necessary for Firefox (its bug), DON'T remove it
        ONEAD_setfocus();

        if (obj.newstate == 'COMPLETED' || obj.newstate == 'DELETED' ){                         
            ONEAD_slide(document.getElementById(ONEAD.wrapper), false, function(){
            removeElement( document.getElementById("div-onead-ad") );
            });
        }
    }
</script>
<!-- OneAD® InPage 結束 -->
<?php endif; ?>
