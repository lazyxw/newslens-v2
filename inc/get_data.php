<?php

class filter_object {
    public $last_day = 5;
    function list_filter( $where ) {
        $where .= " AND post_date > '" . date('Y-m-d', strtotime('-' . $this->last_day . ' days')) . "'";
        return $where;
    }
}

$category__not_in = array();
$slugs            = array ( 'featured', 'marios-digest' );
foreach ( $slugs as $slug ) {
    $cat = get_category_by_slug( $slug );
    $cat and $category__not_in[] = $cat->term_id;
}
define ('EXCLUDE_CATE', serialize ($category__not_in));

function get_popular_list_data($posts_per_page = 10, $paged = 1, $tag = false, $category = false, $author = false, $pagination = false, $last_day = 7){

    $data = array();

    $args = array(
        'meta_key' => 'post_views_count',
        'meta_query'=> array(
          array(
              'key' => 'post_views_count',
              'compare' => '>', 
              'value' => 500,
              'type' => 'numeric'
           )
        ),
        'orderby' => 'rand',
        // 'category__not_in' => unserialize(EXCLUDE_CATE),
     );

    if(is_numeric($posts_per_page)) $args['posts_per_page'] = $posts_per_page;
    if(is_numeric($paged)) $args['paged'] = $paged;
    if(!empty($tag)) $args['tag'] = $tag;
    if(is_numeric($category)) $args['cat'] = $category;
    if(is_numeric($author)) $args['author'] = $author;

    if( $last_day > 0 ) {
        $obj = new filter_object();
        $obj->last_day = $last_day;
        add_filter( 'posts_where', array( $obj, 'list_filter' ) );
    }

    $my_query = new WP_Query( $args );

    $max   = intval( $my_query->post_count );

    if($max < 5){
        // unset($args['meta_query']);
        remove_filter('posts_where', array( $obj, 'list_filter' ) );
        unset($obj);
        $my_query = new WP_Query( $args );
    }

    if( !empty($obj) ) {
        remove_filter('posts_where', array( $obj, 'list_filter' ) );
    }

    if ($my_query->have_posts()) {
        $postCount = 0;

        while ($my_query->have_posts()) {
            $my_query->the_post(); 
            global $post;
            $author_id = $post->post_author;
            $art_thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');
            $thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');

            $is_video = false;
            $categories = get_the_category($post->ID);
            foreach($categories as $category) {
                if ( '影片' == $category->name ){
                    $is_video = true;
                    break;
                }
            }

            $data[] = array(
                'post_id'               => $post->ID,
                'permalink'             => get_permalink(),
                'title'                 => $post->post_title,
                'art_thumb_image_url'   => $art_thumb_image_url[0],
                'thumbnail_image_url'   => $thumbnail_image_url[0],
                'author_link'           => get_author_posts_url(get_the_author_meta( 'ID', $author_id  )),
                'author_name'           => get_the_author_meta( 'display_name', $post->post_author  ),
                'social_count'          => get_post_meta($post->ID, 'social_total_count', $single = true),
                'is_video'              => $is_video,
            );

        }
    }

    wp_reset_query();

    return $data;
}

function get_popular_list($posts_per_page = 10, $paged = 1, $tag = false, $category = false, $author = false, $pagination = false, $last_day = 7){

    $popular_list = get_popular_list_data($posts_per_page, $paged, $tag, $category, $author, $pagination, $last_day);

    if ( count($popular_list) > 0 ) {
        $postCount = 0;
        foreach ( $popular_list as $data){
?>
        <div class="panel">
            <div class="media" style="width:100%;">
                <a class="pull-left" href="<?php echo $data['permalink']; ?>" rel="bookmark" title="<?php echo $data['title']; ?>">
                    <img class="media-object hidden-xs hidden-sm" src="<?php echo $data['art_thumb_image_url']; ?>" style="width: 64px; height: 64px;">
<?php if( $data['is_video'] ): ?>
                    <div class="play-button small-play-button"></div>
<?php endif; ?>
                </a>
                <h4 class="media-heading" style="margin-bottom:5px; line-height:1.3; font-size:17px;"><b><a href="<?php echo $data['permalink']; ?>" rel="bookmark" title="<?php echo $data['title']; ?>" style="color:#444444;"><?php echo $data['title']; ?></a></b></h4>
                <span style="color:#8A8A8A;"><span class="glyphicon glyphicon-user"> <a class="author-link" href="<?php echo $data['author_link']; ?>"><?php echo $data['author_name']; ?></a></span>
                <span class="pull-right" style="padding-top:2px;"><b><?php echo $data['social_count']; ?></b> <i class="glyphicon glyphicon-share"></i></span>
            </div>
        </div>

<?php
            

            $postCount++;

            if(6 == $postCount ){
                if( is_home() ){
                    require('SF_HOME_POPULAR-1ST_300x250.php');
                }else if( is_category() or is_tag() ) {
                    require('SF_CHANNEL_POPULAR-1ST_300x250.php');
                }else if( is_singular() ){
                    require('SF_STORY_SIDEBAR-MIDDLE_300x600.php');
                }
            }
        }
    }
}

function get_popular_list_with_GA($posts_per_page = 23){

    $json = json_decode(get_option( 'tnl_popular_post_list' ));

    if( ! empty($json) ){

        $postCount = 0;

        foreach( $json as $data){

            if($data[0] !== 'www.thenewslens.com') continue;

            preg_match('/[0-9]+/', $data[1], $match);

            $post = get_post($match[0]);

            if( empty($post) ) continue;

            $author_id = $post->post_author;
            $link = get_permalink($post->ID);
            $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');

            $is_video = false;
            $categories = get_the_category($post->ID);
            foreach($categories as $category) {
                if ( '影片' == $category->name ){
                    $is_video = true;
                    break;
                }
            }
?>
        <div class="panel">
            <div class="media" style="width:100%;">
                <a class="pull-left" href="<?php echo $link; ?>" rel="bookmark" title="<?php echo $post->post_title; ?>">
                    <img class="media-object hidden-xs hidden-sm" src="<?php echo $thumb_image_url[0]; ?>" style="width: 64px; height: 64px;">
<?php if( $is_video ): ?>
                    <div class="play-button small-play-button"></div>
<?php endif; ?>
                </a>
                <h4 class="media-heading" style="margin-bottom:5px; line-height:1.3; font-size:17px;"><b><a href="<?php echo $link; ?>" rel="bookmark" title="<?php echo $post->post_title; ?>" style="color:#444444;"><?php echo $post->post_title; ?></a></b></h4>
                <span style="color:#8A8A8A;"><span class="glyphicon glyphicon-user"> <a class="author-link" href="<?php echo get_author_posts_url(get_the_author_meta( 'ID', $author_id  )); ?>"><?php echo get_the_author_meta( 'display_name', $post->post_author  ) ?></a></span>
                <span class="pull-right" style="padding-top:2px;"><b><?php echo get_post_meta($post->ID, 'social_total_count', $single = true) ?></b> <i class="glyphicon glyphicon-share"></i></span>
            </div>
        </div>

<?php
            

            $postCount++;

            if(6 == $postCount ){
                if( is_home() ){
                    require('SF_HOME_POPULAR-1ST_300x250.php');
                }else if( is_category() or is_tag() ) {
                    require('SF_CHANNEL_POPULAR-1ST_300x250.php');
                }else if( is_singular() ){
                    require('SF_STORY_SIDEBAR-MIDDLE_300x600.php');
                }
            }

            if( $postCount == $posts_per_page ){
                break;
            }

        }
    } else {
        get_popular_list($posts_per_page, 1,false, false, false, false, 5);
    }
}

function get_latest_list_data($posts_per_page = 6, $paged = 1, $tag = false, $category = false, $author = false, $pagination = false){
    //popular posts
    $data = array();
    $args = array();

    if(is_numeric($posts_per_page)) $args['posts_per_page'] = $posts_per_page;
    if(is_numeric($paged)) $args['paged'] = $paged;
    if(!empty($tag)) $args['tag'] = $tag;
    if(is_numeric($category)) $args['cat'] = $category;
    if(is_numeric($author)) $args['author'] = $author;
    // $args['category__not_in'] = unserialize(EXCLUDE_CATE);

    $my_query = new WP_Query($args);
    if($pagination) return $my_query;

    if ($my_query->have_posts()) {
        $postCount = 0;
        while ($my_query->have_posts()){
            $my_query->the_post(); 
            global $post;
            $author_id = $post->post_author;
            $art_thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');
            $thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');

            $is_video = false;
            $categories = get_the_category($post->ID);
            foreach($categories as $category) {
                if ( '影片' == $category->name ){
                    $is_video = true;
                    break;
                }
            }

            $desc = summarize_long_text($post->post_content, 160);
            $timestamp = strtotime(get_gmt_from_date($post->post_date));

            $data[] = array(
                'post_id'               => $post->ID,
                'permalink'             => get_permalink(),
                'title'                 => $post->post_title,
                'desc'                  => $desc,
                'timestamp'             => $timestamp,
                'art_thumb_image_url'   => $art_thumb_image_url[0],
                'thumbnail_image_url'   => $thumbnail_image_url[0],
                'author_link'           => get_author_posts_url(get_the_author_meta( 'ID', $author_id  )),
                'author_name'           => get_the_author_meta( 'display_name', $post->post_author  ),
                'social_count'          => get_post_meta($post->ID, 'social_total_count', $single = true),
                'comment_count'         => $post->comment_count,
                'is_video'              => $is_video,
            );
        }
    }

    wp_reset_query();

    return $data;
}

function get_latest_list($posts_per_page = 6, $paged = 1, $tag = false, $category = false, $author = false, $pagination = false){
    $latest_list = get_latest_list_data($posts_per_page, $paged, $tag, $category, $author, $pagination);

    if ( count($latest_list) > 0 ) {
        if(!$pagination){

            $postCount = 0;
            foreach ( $latest_list as $data){
?>
        <div class="panel">
            <div class="media" style="min-height:150px;">
                <a class="pull-left" href="<?php echo $data['permalink']; ?>" title="<?php echo $data['title']; ?>">
                    <img class="media-object hidden-xs hidden-sm" src="<?php echo $data['thumbnail_image_url']; ?>" style="width: 150px; height: auto;">
<?php if( $data['is_video'] ): ?>
                    <div class="play-button medium-play-button"></div>
<?php endif; ?>
                </a>
                <h3 class="media-heading"><b><a href="<?php echo $data['permalink'] ?>" style="color:#444444;"><?php echo $data['title']; ?></b></h3>
                <div class="media-body"><?php echo $data['desc']; ?> ...</div></a>
            </div>
            <div class="panel-footer">
                作者：<a class="author-link" href="<?php echo $data['author_link'] ?>"><?php echo $data['author_name']; ?></a>
                <span class="pull-right" style="padding-left:10px;"><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></span>
                <span class="pull-right" style="padding-top:2px;"><b><?php echo $data['comment_count']; ?></b> <i class="glyphicon glyphicon-comment"></i></span>
                <span class="pull-right" style="padding:2px 8px 0 0;"><b><?php echo $data['social_count']; ?></b> <i class="glyphicon glyphicon-share"></i></span>
            </div>
        </div>
<?php
            

                $postCount++;

                if(1 == $postCount ){
                    if( is_home() ){
                        require('SF_HOME_LATEST-1ST_300x250.php');
                    }else{
                        require('SF_CHANNEL_LATEST-1ST_300x250.php');
                    }
                } else if ($posts_per_page == $postCount){
                    if( is_home() ){
                        require('SF_HOME_LATEST-BOTTOM_300x250.php');
                    }else{
                        require('SF_CHANNEL_LATEST-BOTTOM_300x250.php');
                    }
                }
            }
        } else {
            wpbeginner_numeric_posts_nav($latest_list);
        }
    } else {
?>
        <div class="panel">
            <div class="media">
                沒有相關文章。
            </div>
        </div>
<?php
    }
}




function get_featured_list($posts_per_page = 5, $paged = 1, $last_day = 7){
    $args = array(
        'category_name' => 'featured',
        'posts_per_page' => $posts_per_page,
        'orderby' => 'rand',
        'paged' => $paged,
     );

    if ( $last_day > 0){
        $obj = new filter_object();
        $obj->last_day = $last_day;
        add_filter( 'posts_where', array( $obj, 'list_filter' ) );
    }

    $my_query = new WP_Query( $args );

    $max   = intval( $my_query->post_count );

    if($max < 5){
        // unset($args['meta_query']);
        remove_filter('posts_where', array( $obj, 'list_filter' ) );
        unset($obj);
        $my_query = new WP_Query( $args );
    }

    if( !empty($obj) ) {
        remove_filter('posts_where', array( $obj, 'list_filter' ) );
    }


    if ($my_query->have_posts()) {
        $postCount = 0;
        while ($my_query->have_posts()) : 
            $my_query->the_post(); 
            global $post;
            $author_id = $post->post_author;
            $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'side');

            $is_video = false;
            $categories = get_the_category($post->ID);
            foreach($categories as $category) {
                if ( '影片' == $category->name ){
                    $is_video = true;
                    break;
                }
            }

            $desc = summarize_long_text($post->post_content);
?>
        <div class="panel">
            
            <div class="media">
                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                    <img class="media-object hidden-xs hidden-sm"src="<?php echo $thumb_image_url[0]; ?>" style="width: 100%; height:auto">
<?php if( $is_video ): ?>
                    <div class="play-button responsive-play-button"></div>
<?php endif; ?>
                </a>
                <h4 class="media-heading" style="margin-bottom:5px; line-height:1.3; font-size:17px;"><b><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" style="color:#444444;"><?php echo $post->post_title; ?></a></b></h4>
                <div class="media-body"><?php echo $desc; ?> ...</a></div>
            </div>
            <div class="panel-footer"> 
                作者：<a class="author-link" href="<?php echo get_author_posts_url(get_the_author_meta( 'ID', $author_id  )); ?>"><?php echo get_the_author_meta( 'display_name', $post->post_author  ) ?></a>
            </div>
        </div>

<?php
            

            $postCount++;

        endwhile; 
    }

    wp_reset_query();
}


function get_mario_digest($posts_per_page = 6, $paged = 1, $pagination = false){
    $args = array(
        'post_type' => 'buzz',
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
     );

    $my_query = new WP_Query( $args );

    if ($my_query->have_posts()) {
        if(!$pagination){
            $postCount = 0;
            while ($my_query->have_posts()) :
                $my_query->the_post(); 
                global $post;

                $timestamp = strtotime(get_gmt_from_date($post->post_date));
                $url = get_post_meta( $post->ID, 'buzz_url');
                $summary = get_post_meta( $post->ID, 'buzz_summary');
                $buzz_source = esc_html( get_post_meta( $post->ID, 'buzz_source', true ) );
                $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');
?>
        <div class="panel">
            <div class="media" style="width:100%;">
                <a class="pull-left" href="<?php echo $url[0]; ?>" rel="bookmark" title="<?php echo $post->post_title; ?>"><img src="<?php echo $thumb_image_url[0]; ?>" style="width: 64px; height: 64px;"></a>
                <h4 class="media-heading"><b><a href="<?php echo $url[0]; ?>" style="color:#444444;" target="_blank"><?php echo $post->post_title; ?></a></b></h4>
                <div class="media-body"><?php echo $summary[0]; ?></div>
                <span class="pull-right" style="padding-left:10px;"><abbr class="timeago" title="<?php echo date('c', $timestamp); ?>"><?php echo date('Y/m/d', $timestamp); ?></abbr></span>
                <?php if( ! empty( $buzz_source ) ): ?>
                <span class="pull-right" style="padding-left:10px;">來源: <?php echo $buzz_source; ?></span>
                <?php endif; ?>
            </div>
        </div>
<?php

                $postCount++;

            endwhile;

        } else {
            wpbeginner_numeric_posts_nav($my_query, true);
        }
    }

    wp_reset_query();
}

function get_mario_digest_short(){
    $cate_name = '馬力歐報報';

    $args = array(
        'post_type' => 'buzz',
        'posts_per_page' => 3,
     );

    $my_query = new WP_Query( $args );

    if ($my_query->have_posts()) {
?>
        <div class="panel">
            <div class="panel-heading"><?php echo $cate_name; ?></div>
            <div class="media" style="width:100%;">
<?php
        $postCount = 0;
        while ($my_query->have_posts()) :
            $my_query->the_post(); 
            global $post;
            $timestamp = strtotime(get_gmt_from_date($post->post_date));

            $url = get_post_meta( $post->ID, 'buzz_url');
            $summary = get_post_meta( $post->ID, 'buzz_summary');
?>
            <div class="media" style="width:100%;">
                <b><a href="<?php echo $url[0]; ?>" target="_blank"><?php echo $post->post_title; ?></a></b>
                <div class="media-body"><?php echo $summary[0]; ?></div>
                <span class="pull-right" style="padding-left:10px;"><abbr class="timeago" title="<?php echo date('c', $timestamp); ?>"><?php echo date('Y/m/d', $timestamp); ?></abbr></span>
            </div>
<?
            $postCount++;

        endwhile;

?>
                <div style="text-align:right;margin-top:5px;"><a href="/buzz/">更多<?php echo $cate_name; ?></a></div>
            </div>
        </div>
<?php
    }

    wp_reset_query();
}

function get_recent_comments(){
    $block_title = '最新回應';
    $comment_length = 40;
    $comments = array();

    $args = array(
        'status'        => 'approve',
        'number'        => 30,
        'meta_key'      => 'dsq_post_id',
     );

    $recent_comments = get_comments($args);

    foreach($recent_comments as $obj){
        if( empty( $comments[$obj->comment_post_ID] ) ){
            $comments[$obj->comment_post_ID] = $obj;
        }

        if( count($comments) >= 3 ) break;
    }

    if ( count($comments) > 0 ) {
?>
        <div class="panel">
            <div class="panel-heading"><?php echo $block_title; ?></div>
            <div class="media" style="width:100%;">
<?php
        foreach ( $comments as $obj ) {
            $post_id = $obj->comment_post_ID;
            $post = get_post( $post_id );
            $timestamp = strtotime(get_gmt_from_date($obj->comment_date_gmt));
            $url = get_permalink( $post_id ) . '?ref=rc#comment-' . $obj->meta_value;
            $summary = $obj->comment_author . ': ' . summarize_long_text ( $obj->comment_content, $comment_length ) . ((mb_strlen( $obj->comment_content ) > $comment_length)?' ...':'');
?>
            <div class="media" style="width:100%;">
                <b><a href="<?php echo $url; ?>"><?php echo $post->post_title; ?></a></b>
                <div class="media-body"><?php echo $summary; ?></div>
                <span class="pull-right" style="padding-left:10px;"><abbr class="timeago" title="<?php echo date('c', $timestamp); ?>"><?php echo date('Y/m/d', $timestamp); ?></abbr></span>
            </div>
<?

        }

?>
            </div>
        </div>
<?php
    }

    wp_reset_query();
}


function menu_login_logout() {
    global $current_user; 
    get_currentuserinfo();

    if (is_user_logged_in())
    {
        $log_out = '<a rel="nofollow" href="'. wp_logout_url( home_url() ) . '" title="Logout"><button type="button" class="btn btn-primary">登出</button></a>';
        return $log_out;
    }
    else 
    {
        $log_in = '<a rel="nofollow" href="http://www.thenewslens.com/wp-login.php?action=wordpress_social_authenticate&amp;provider=Facebook&amp;redirect_to=http%3A%2F%2Fwww.thenewslens.com%2Fwp-login.php%3Floggedout%3Dtrue" title="Connect with Facebook" class="wsl_connect_with_provider">
                            <button type="button" class="btn btn-primary">facebook 登入</button></a>';
        return $log_in;
    }
    wp_reset_query();
}
    
