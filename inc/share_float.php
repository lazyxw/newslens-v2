<div style="text-align:center;">
    <iframe id="share-gp" data-src="https://plusone.google.com/_/+1/fastbutton?bsv&amp;size=tall&amp;hl=zh-TW&amp;url=<?= urlencode(esc_url(get_permalink())) ?>&amp;parent=<?= urlencode(get_home_url()) ?>" allowtransparency="true" frameborder="0" scrolling="no" title="+1" style="border:none; overflow:hidden; width:63px; height:68px; padding-top:7px;"></iframe>
    <iframe id="share-fb" data-src="//www.facebook.com/plugins/like.php?href=<?= urlencode(esc_url(get_permalink()))?>&amp;send=false&amp;layout=box_count&amp;width=54&amp;show_faces=false&amp;colorscheme=light&amp;action=like&amp;height=61" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:54px; height:61px;" allowTransparency="true"></iframe>
    <iframe id="share-tw" allowtransparency="true" frameborder="0" scrolling="no" data-src="http://platform.twitter.com/widgets/tweet_button.1374787011.html#_=1374834930740&amp;count=vertical&amp;id=twitter-widget-0&amp;lang=zh-TW&amp;original_referer=<?= urlencode(get_home_url()) ?>&amp;size=m&amp;text=<?= urlencode(get_the_title()) ?>&amp;url=<?= urlencode(esc_url(wp_get_shortlink())) ?>&amp;counturl=<?= urlencode(esc_url(get_permalink())) ?>" class="twitter-share-button twitter-count-vertical" title="Twitter Tweet Button" data-twttr-rendered="true" style="width: 59px; height: 62px;"></iframe>
    <div class="share share_size_large share_type_comment">
        <span class="share__count">
        <?php
            global $post;
            echo $post->comment_count;
        ?>
        </span>
        <a class="share__btn" href="#comment-panel">留言</a>
    </div>
</div> 

