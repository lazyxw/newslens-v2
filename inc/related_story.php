<?php ob_start(); ?>
    <hr>
    <div>
<?php
    $stories_items = 5;
    $tracking_latest = '?ref=cl';
    $tracking_popular = '?ref=cp';

    $latest_stories = get_latest_list_data($stories_items);

    if( count($latest_stories) ){
?>
        <div class="col-lg-6">
            <div>
                <div class=""><h4><b>最新文章</b></h4></div>
                <div class="media" style="width:100%;">
<?php foreach($latest_stories as $data): ?>
                    <div class="media"><a class="pull-left" href="<?php echo $data['permalink'] . $tracking_latest; ?>" title="<?php echo $data['title']; ?>"><img class="media-object" src="<?php echo $data['thumbnail_image_url']; ?>" height="64px" width="64px"></a><div class="media-body"><h4 class="media-heading article-title"><a href="<?php echo $data['permalink'] . $tracking_latest; ?>"><?php echo $data['title']; ?></a></h4></div></div>
<?php endforeach; ?>
                </div>
            </div>
        </div>
<?php
    }

    $popular_stories = json_decode(get_option( 'tnl_popular_post_list' ));
    $popular_stories_count = $stories_items;

    if( ! empty($popular_stories) and count($popular_stories) > 0 ){
        $postCount = 0;

?>
       <div class="col-lg-6">
            <div>
                <div class=""><h4><b>熱門文章</b></h4></div>
                <div class="media" style="width:100%;">
<?php 
    foreach($popular_stories as $data): 
        if($data[0] !== 'www.thenewslens.com') continue;

        preg_match('/[0-9]+/', $data[1], $match);

        $current_post = get_post($match[0]);

        if($current_post === false) continue;

        $author_id = $current_post->post_author;
        $link = get_permalink($current_post->ID);
        $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($current_post->ID), 'art-thumb');

?>
                    <div class="media"><a class="pull-left" href="<?php echo $link . $tracking_popular; ?>"><img class="media-object" src="<?php echo $thumb_image_url[0]; ?>" height="64px" width="64px" alt="<?php echo $current_post->post_title; ?>"></a><div class="media-body"><h4 class="media-heading article-title"><a href="<?php echo $link . $tracking_popular; ?>"><?php echo $current_post->post_title; ?></a></h4></div></div>
<?php
        $postCount++;

        if( $postCount == $popular_stories_count ) break;
    endforeach;
?>
                </div>
            </div>
        </div>
<?php
    }
?>
    </div>
    <br clear="all">
<?php
$output = ob_get_clean();
echo preg_replace('/\s\s+/', '', $output);
