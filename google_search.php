<?php
/*
    Template Name: google custom search
*/
?>
<?php get_header(); ?>

    <div id="left-sidebar" class="col-lg-2 visible-lg" style="">
            
    </div>

    <div id="main-content" class="col-lg-8 col-sm-8" style="padding-top:20px;min-height:300px;">
<style media="screen" type="text/css">

.gsc-control-cse .gs-result .gs-title, .gsc-control-cse .gs-result .gs-title * {
font-size: 16px;
line-height: 20px !important;
}

.gsc-input-box {
-webkit-box-shadow: none !important;
box-shadow: none !important;
min-height: 32px !important;
}
.gsc-input-box-hover{
-webkit-box-shadow: none !important;
box-shadow: none !important;
}
.gsc-input-box-focus {
-webkit-box-shadow: none !important;
box-shadow: none !important;
}

.gsc-selected-option{
    min-width: 40px !important;
}

.gsc-search-button .gsc-search-button-v2 {
    height:27px;
}


</style>
        <script>
          (function() {
            var cx = '004951195181085949750:gkeq2dwrqvu';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = true;
            gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                '//www.google.com/cse/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
          })();
        </script>
        <gcse:searchbox></gcse:searchbox>
        <gcse:searchresults></gcse:searchresults>
    </div>

    <div id="right-top-sidebar" class="col-lg-2 col-sm-4 hidden-xs hidden-sm" style="">
        
    </div>

        


<?php get_footer(); ?>