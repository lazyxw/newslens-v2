<?php
/*
    Template Name: sponsor page
*/
?>
<?php get_header(); ?>

    <div id="left-sidebar" class="col-lg-2 visible-lg">
            
    </div>

    <div id="main-content" class="panel col-lg-8 col-sm-8" style="padding:20px;border-style: solid;border-width: 5px;border-color: #B6B6B6;">
        <div style="float: right;background-color: #B6B6B6;margin-top: -20px;margin-right: -25px;padding: 0 11px;border-bottom-left-radius: 2px;color: #fff;">廣編企劃</div>
            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
                    </div><!-- .entry-content -->

                </article><!-- #post -->

            <?php endwhile; ?>
    </div>

    <div id="right-top-sidebar" class="col-lg-2 col-sm-4 hidden-xs hidden-sm" style="">
        
    </div>

<?php get_footer(); ?>