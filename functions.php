<?php

require_once('inc/helper.php');
require_once('inc/get_data.php');


function tnl_load_css() {
    if (is_admin()) return;

    // wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    // wp_enqueue_style('tnl2', get_template_directory_uri() . '/assets/css/tnl2.css');
    // wp_enqueue_style('bootstrap-glyphicons', get_template_directory_uri() . '/assets/css/bootstrap-glyphicons.css');
    // wp_enqueue_style('flyout-post-nav', get_template_directory_uri() . '/assets/css/flyout-post-nav.css');
    wp_enqueue_style('tnl-pack', get_template_directory_uri() . '/assets/css/tnl-pack.min.css');
}
add_action('init', 'tnl_load_css');


function tnl_load_js() {

    if (is_admin()) return;

    wp_enqueue_script('jquery', false, false, 1, false);
    // wp_enqueue_script('jquery.timeago', get_template_directory_uri() . '/assets/js/jquery.timeago.js', array( 'jquery' ), 1, true);
    // wp_enqueue_script('jquery.timeago.zh-TW', get_template_directory_uri() . '/assets/js/jquery.timeago.zh-TW.js', array( 'jquery', 'jquery.timeago' ), 1, true);
    // wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', false, 1, true);
    // wp_enqueue_script( 'tnl', get_template_directory_uri() . '/assets/js/tnl.js', array('bootstrap'), 1, true);
    //JS: top
    // wp_enqueue_script('easing', get_template_directory_uri() . '/assets/js/jquery.easing.1.3.js','', 1, true);
    // wp_enqueue_script('jstop', get_template_directory_uri() . '/assets/js/jquery.ui.totop.js','', 1, true);
    
    // JS: cookie
    // wp_enqueue_script('jquery-cookie', get_template_directory_uri() . '/assets/js/jquery.cookie.js', array( 'jquery' ), 1, false);

    // JS: traditional chinese / simplified chinese converter
    // wp_enqueue_script('tcsc-converter', get_template_directory_uri() . '/assets/js/langs.js','', 1, false);

    // JS: full background
    // wp_enqueue_script('jquery.ez-bg-resize', get_template_directory_uri() . '/assets/js/jquery.ez-bg-resize.js', array( 'jquery' ), 1, true);

    // JS: flyout post nav
    // wp_enqueue_script('jquery.flyout-post-nav', get_template_directory_uri() . '/assets/js/jquery.nextPost.js', array( 'jquery' ), 1, true);

    // JS: traditional chinese / simplified chinese converter
    // wp_enqueue_script('floating-share', get_template_directory_uri() . '/assets/js/diggdigg-floating-bar.js','', 1, false);

    wp_enqueue_script('tnl-pack', get_template_directory_uri() . '/assets/js/tnl-pack.min.js','', 1, true);
}
add_action('init', 'tnl_load_js');

// add extra contact info
function extra_contact_info($contactmethods) {
    unset($contactmethods['aim']);
    unset($contactmethods['yim']);
    unset($contactmethods['jabber']);
    
    $contactmethods['linkedin'] = 'linkedIn';
    $contactmethods['youtube'] = 'youtube';
    
    return $contactmethods;
}
add_filter('user_contactmethods', 'extra_contact_info');


// remove admin bar in frontend
add_filter('show_admin_bar', '__return_false');

//set the content width
if ( ! isset( $content_width ) ) $content_width = 720;


//the image sizes that we use
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size(66, 66, true );
add_image_size('art-thumb', 66, 66, true);
add_image_size('art-big', 720, 375, true);
add_image_size('art-big-2col', 346, 154, true);
add_image_size('art-gal', 220, 220, true);
add_image_size('side', 300, 110, true);
add_image_size('arhive', 220, 132, true);



// Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'feed_links', 2 ); 


// add rewrite rule for author link - support /author/author_nicename
function wpdocs_author_rewrites( $author_rewrite ) {
    $contributors = array(
        'author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$' => 'index.php?author_name=$matches[1]&feed=$matches[2]',
        'author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$'      => 'index.php?author_name=$matches[1]&feed=$matches[2]',
        'author/([^/]+)/page/?([0-9]{1,})/?$'             => 'index.php?author_name=$matches[1]&paged=$matches[2]',
        'author/([^/]+)/?$'                               => 'index.php?author_name=$matches[1]'
    );
 
    $old = array();
    // This relies on you registering a 'redirect' query var and handler.
    // The handler would check for 'redirect' and apply a wp_redirect call where necessary.
    foreach ( $author_rewrite as $endpoint => $query )
        $old[$endpoint] = $query . '&redirect=1';
 
    return array_merge( $old, $contributors );
}
add_filter( 'author_rewrite_rules', 'wpdocs_author_rewrites' );

// rewrite author url - remove /post
function my_author_url_with_id($link, $author_id, $author_nicename) {
  $link_base = trailingslashit(get_option('home'));
  $link = "author/$author_nicename/";
  return $link_base . $link;
}
add_filter('author_link', 'my_author_url_with_id', 1000, 3);

// Modifies RSS permalinks to use Google Analytics tracking
function trackedrss($guid) {
    if (is_feed()) {
        if(!empty($_GET['from'])){
            $operator = (preg_match("/\?/i", $guid)) ? "&amp;":"?";
            $format = "%s"."utm_source=%s"."&amp;utm_medium=%s"."&amp;utm_campaign=%s";
            $url_pre = $guid . $operator;
            $source = strip_tags($_GET['from']);
            $medium = "feed";
            $campaign = '3rd+party';
            return sprintf($format,$url_pre,$source,$medium,$campaign);
        } else {
            return $guid;
        }
    }
}
add_filter ('the_permalink_rss','trackedrss');

// insert fan page like to direct after content
function fan_page_like($content){
    if(is_single()){
        $extra_html = '';

        $extra_html .= '<div style="border:#1ba1e2 3px dotted;border-top:1px;margin: 10px 0;"></div>';
        $extra_html .= '<div style="font-size:small;margin:3px 0 0 0;vertical-align:top;line-height:24px;"><iframe src="//www.facebook.com/plugins/like.php?href=' . urldecode('http://www.facebook.com/TheNewsLens') . '&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;colorscheme=light&amp;action=like&amp;height=24" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:24px;" allowTransparency="true"></iframe> 如果你喜歡我們的分享和文章，請幫我們按個讚</div>';
        $extra_html .= '<div style="border:#1ba1e2 3px dotted;border-top:1px;margin: 0 0 10px 0;"></div>';
        $extra_html .= '[shareaholic app="share_buttons" id="607687"]';

        // ob_start();
        // require('inc/SF_STORY_BELOW-CONTENT_728x90.php');
        // require('inc/related_story.php');
        // $extra_html .= ob_get_clean();

        $content .= $extra_html;
    }
    return $content;
}
add_filter( "the_content", "fan_page_like", 1);

// remove backspace character in content for feed
function clearChr($content) {
    if(is_feed()){
        $content = str_replace(chr(8), '', $content);
    }
    return $content;
}
add_filter('the_content', 'clearChr');

// redirect /post to home
function my_404_override() {
    global $wp_query;

    if ( is_404() ) {
        $redirect = parse_url($_SERVER['REQUEST_URI']);

        if ( '/post/' == $redirect['path'] or '/post' == $redirect['path']){
            status_header( 200 );
            $wp_query->is_404=false;
            wp_redirect( home_url(), 301 ); 
            exit;
        }
    }
}
add_filter('template_redirect', 'my_404_override' );

// filter mario's buzz for HTC
function filter_buzz_where( $where ){
    if( is_feed() and !empty($_GET['from']) and $_GET['from'] == 'htc' ) {
        $where .= ' AND post_title NOT LIKE "%馬力歐%"';
    }

  return $where;
}
add_filter('posts_where', 'filter_buzz_where' );

// use summary for kollect's feed
function summary_feed( $content ){
    if( is_feed() and !empty($_GET['from']) and $_GET['from'] == 'kollect' ) {
        $content = summarize_long_text($content, 200) . '...';
    }

  return $content;
}
add_filter('the_content', 'summary_feed' );

// add feed for opinion only, news only, video only
function feedAboutNews($query) {
    if( is_feed() ) {
        if( !empty($_GET['ro']) ) {
            $query->set('cat','-2758,-12049');
        } else if( !empty($_GET['no']) ) {
            $query->set('cat','12049');
        } else if( !empty($_GET['vo']) ) {
            $query->set('cat','2758');
        }
    }
    // return $query;
}
add_filter('pre_get_posts','feedAboutNews');

// feed for 4free wifi
function feedFor4free($content) {
    if( is_feed() and !empty($_GET['from']) and $_GET['from'] == '4free' ) {
        // Global $post variable
        global $post;
        // Check if the post has a featured image
        if (has_post_thumbnail($post->ID)) {
            $content = '<div class="content_summary">' . summarize_long_text($content, 160) . '...</div>';
            $content = '<div class="featured_image_post_rss">' . get_the_post_thumbnail($post->ID, 'full') . '</div>' . $content;
        }
    }
    
    return $content;
}

//Add the filter for RSS feeds Excerpt
add_filter('the_excerpt_rss', 'feedFor4free');
//Add the filter for RSS feed content
add_filter('the_content_feed', 'feedFor4free');

// filter meta desc for yoast seo
function filter_meta_desc( $str ) {
  return preg_replace('/Photo(.*?)2.0/i', '', $str);
}
add_filter( 'wpseo_metadesc', 'filter_meta_desc', 10, 1 );

// Stop WordPress Editor from Removing iFrame and Embed Code
function mytheme_tinymce_config( $init ) {
    $valid_iframe = 'iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]';
    if ( isset( $init['extended_valid_elements'] ) ) {
        $init['extended_valid_elements'] .= ',' . $valid_iframe;
    } else {
        $init['extended_valid_elements'] = $valid_iframe;
    }
    return $init;
}
add_filter('tiny_mce_before_init', 'mytheme_tinymce_config');
