<?php 
if(isset($_GET['abid'])){
    if('subscribenews' == $_GET['abid']){
        $ml_id      = 'b15819b0b0';
        $ml_name    = 'newsletter';
        $form_title = '訂閱 The News Lens 電子報';
        $form_desc  = '訂閱電子報後您將會在每周一至周五收到當日最新的文章。';
    }elseif('subscribeauthor' == $_GET['abid']){
        $ml_id      = '70a0b80662';
        $ml_name    = 'author';
        $form_title = '訂閱作者文章';
        $form_desc  = '此功能尚未完成，如果您有意願使用此功能，請輸入您的 E-mail，我們將在功能上線時通知您。';
    }elseif('subscribecategory' == $_GET['abid']){
        $ml_id      = '5232f5343b';
        $ml_name    = 'category';
        $form_title = '訂閱這個分類的文章';
        $form_desc  = '此功能尚未完成，如果您有意願使用此功能，請輸入您的 E-mail，我們將在功能上線時通知您。';
    }elseif('subscribetopic' == $_GET['abid']){
        $ml_id      = '3b2e5bbbea';
        $ml_name    = 'topic';
        $form_title = '訂閱相同標籤的文章';
        $form_desc  = '此功能尚未完成，如果您有意願使用此功能，請輸入您的 E-mail，我們將在功能上線時通知您。';
    }

    if(!empty($ml_id)){
?>

<!--Leave us your email-->
<div class="panel">
<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
</style>
<div id="mc_embed_signup">
<form action="http://duablechinese.us6.list-manage.com/subscribe/post?u=9b25500f2a71c2a4131f5e515&amp;id=<?php echo $ml_id; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <h4><?php echo $form_title; ?></h4>
 <p style="font-color:grey;"><?php echo $form_desc; ?></p>
<div class="mc-field-group">
    <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
    <div id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
    </div>  <div class="clear"><input type="submit" value="訂閱" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</form>
</div>
<script type="text/javascript">
var _gaq = _gaq || [];
var fnames = new Array();var ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';
try {
    var jqueryLoaded=jQuery;
    jqueryLoaded=true;
} catch(err) {
    var jqueryLoaded=false;
}
var head= document.getElementsByTagName('head')[0];
if (!jqueryLoaded) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = '//ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
    head.appendChild(script);
    if (script.readyState && script.onload!==null){
        script.onreadystatechange= function () {
              if (this.readyState == 'complete') mce_preload_check();
        }    
    }
}

var err_style = '';
try{
    err_style = mc_custom_error_style;
} catch(e){
    err_style = '#mc_embed_signup input.mce_inline_error{border-color:#6B0505;} #mc_embed_signup div.mce_inline_error{margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff;}';
}
var head= document.getElementsByTagName('head')[0];
var style= document.createElement('style');
style.type= 'text/css';
if (style.styleSheet) {
  style.styleSheet.cssText = err_style;
} else {
  style.appendChild(document.createTextNode(err_style));
}
head.appendChild(style);
setTimeout('mce_preload_check();', 250);

var mce_preload_checks = 0;
function mce_preload_check(){
    if (mce_preload_checks>40) return;
    mce_preload_checks++;
    try {
        var jqueryLoaded=jQuery;
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'http://downloads.mailchimp.com/js/jquery.form-n-validate.js';
    head.appendChild(script);
    try {
        var validatorLoaded=jQuery("#fake-form").validate({});
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    mce_init_form();
}
function mce_init_form(){
    jQuery(document).ready( function(jQuery) {
      var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
      var mce_validator = jQuery("#mc-embedded-subscribe-form").validate(options);
      jQuery("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
      options = { url: 'http://duablechinese.us6.list-manage1.com/subscribe/post-json?u=9b25500f2a71c2a4131f5e515&id=<?php echo $ml_id; ?>&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
                    beforeSubmit: function(){
                        jQuery('#mce_tmp_error_msg').remove();
                        jQuery('.datefield','#mc_embed_signup').each(
                            function(){
                                var txt = 'filled';
                                var fields = new Array();
                                var i = 0;
                                jQuery(':text', this).each(
                                    function(){
                                        fields[i] = this;
                                        i++;
                                    });
                                jQuery(':hidden', this).each(
                                    function(){
                                        var bday = false;
                                        if (fields.length == 2){
                                            bday = true;
                                            fields[2] = {'value':1970};//trick birthdays into having years
                                        }
                                        if ( fields[0].value=='MM' && fields[1].value=='DD' && (fields[2].value=='YYYY' || (bday && fields[2].value==1970) ) ){
                                            this.value = '';
                                        } else if ( fields[0].value=='' && fields[1].value=='' && (fields[2].value=='' || (bday && fields[2].value==1970) ) ){
                                            this.value = '';
                                        } else {
                                            if (/\[day\]/.test(fields[0].name)){
                                                this.value = fields[1].value+'/'+fields[0].value+'/'+fields[2].value;                                           
                                            } else {
                                                this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
                                            }
                                        }
                                    });
                            });
                        jQuery('.phonefield-us','#mc_embed_signup').each(
                            function(){
                                var fields = new Array();
                                var i = 0;
                                jQuery(':text', this).each(
                                    function(){
                                        fields[i] = this;
                                        i++;
                                    });
                                jQuery(':hidden', this).each(
                                    function(){
                                        if ( fields[0].value.length != 3 || fields[1].value.length!=3 || fields[2].value.length!=4 ){
                                            this.value = '';
                                        } else {
                                            this.value = 'filled';
                                        }
                                    });
                            });
                        _gaq.push(['_trackEvent', 'subscribe', 'click', '<?php echo $ml_name; ?>', 1]);
                        return mce_validator.form();
                    }, 
                    success: mce_success_cb
                };
      jQuery('#mc-embedded-subscribe-form').ajaxForm(options);
      
      
    });
}
function mce_success_cb(resp){
    
    jQuery('#mce-success-response').hide();
    jQuery('#mce-error-response').hide();
    if (resp.result=="success"){
        _gaq.push(['_trackEvent', 'subscribe', 'success', '<?php echo $ml_name; ?>', 1]);

        jQuery('#mce-'+resp.result+'-response').show();
        jQuery('#mce-'+resp.result+'-response').html(resp.msg);
        jQuery('#mc-embedded-subscribe-form').each(function(){
            this.reset();
        });
    } else {
        _gaq.push(['_trackEvent', 'subscribe', 'failed', '<?php echo $ml_name; ?>', ]);
        var index = -1;
        var msg;
        try {
            var parts = resp.msg.split(' - ',2);
            if (parts[1]==undefined){
                msg = resp.msg;
            } else {
                i = parseInt(parts[0]);
                if (i.toString() == parts[0]){
                    index = parts[0];
                    msg = parts[1];
                } else {
                    index = -1;
                    msg = resp.msg;
                }
            }
        } catch(e){
            index = -1;
            msg = resp.msg;
        }
        try{
            if (index== -1){
                jQuery('#mce-'+resp.result+'-response').show();
                jQuery('#mce-'+resp.result+'-response').html(msg);            
            } else {
                err_id = 'mce_tmp_error_msg';
                html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
                
                var input_id = '#mc_embed_signup';
                var f = jQuery(input_id);
                if (ftypes[index]=='address'){
                    input_id = '#mce-'+fnames[index]+'-addr1';
                    f = jQuery(input_id).parent().parent().get(0);
                } else if (ftypes[index]=='date'){
                    input_id = '#mce-'+fnames[index]+'-month';
                    f = jQuery(input_id).parent().parent().get(0);
                } else {
                    input_id = '#mce-'+fnames[index];
                    f = jQuery().parent(input_id).get(0);
                }
                if (f){
                    jQuery(f).append(html);
                    jQuery(input_id).focus();
                } else {
                    jQuery('#mce-'+resp.result+'-response').show();
                    jQuery('#mce-'+resp.result+'-response').html(msg);
                }
            }
        } catch(e){
            jQuery('#mce-'+resp.result+'-response').show();
            jQuery('#mce-'+resp.result+'-response').html(msg);
        }
    }
}

</script>
<!--End mc_embed_signup-->
</div>
<?php
    }
}
?>