<?php get_header(); ?>
<div id="div-onead-ad" class="visible-lg" style="padding-top:68px;">
<script type="text/javascript">
    ONEAD.cmd.push(function(){
        ONEAD_slot('div-onead-ad');
    });
</script>
</div>
<div>
    <div id="left-sidebar" class="col-lg-1 hidden-sm hidden-xs">
    </div>
</div>

<div id="main-content" class="col-lg-7 col-sm-8">
<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'content', get_post_format() ); ?>
<?php endwhile; ?>
</div>

<div id="right-top-sidebar" class="col-lg-3 col-sm-4 hidden-sm hidden-xs">
    <?php // require_once('inc/highlight_tag.php'); ?>
    <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>

    <div class="panel">
        <?php get_template_part( 'author-bio' ); ?>
    </div>
	<div class="panel" style="background-color:#000000; color:#fff;">
	作者其他文章
	</div>
    <?php get_popular_list(10, 1,false, false, $post->post_author, false, 0); ?>
    <?php require('inc/fb_recommend.php'); ?>
</div>

<?php get_footer(); ?>