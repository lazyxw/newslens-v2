        <div class="panel-heading">
            作者
<?php
    $author_type = get_the_author_meta('reprint');
    if( 1 == $author_type ){
        echo ' (合作轉載)';
    }
?>
        </div>
        <!-- Default panel contents -->
        <div class="media" style="width:100%;">
<?php
    $html_avatar = get_avatar_url(get_avatar( $post->post_author, 64 ));
?>
            <img class="media-object img-circle" src="<?php echo $html_avatar; ?>" style="width: 64px; height: 64px; margin: 0 auto 0 auto; padding:3px;">

            <h4 class="media-heading text-center" style="padding:10px;">
                <b><?php coauthors_posts_links(' ', ' '); ?></b>
            </h4>
            <div class="media-body"><?php echo str_replace("\n", '<br>', get_the_author_meta( 'description' )); ?></div>
            <div class="author-social">
<?php
author_add_social('facebook');
author_add_social('googleplus');
author_add_social('twitter');
author_add_social('linkedin');
author_add_social('youtube');
?>
            </div>
            <div class="author-website">
<?php
                $authorWebsite = get_the_author_meta('url');
                if (!empty($authorWebsite)) {
                    echo '<a href="' . $authorWebsite . '" target="_blank">' . $authorWebsite . '</a>';
                }
?>
            </div>
        </div>
