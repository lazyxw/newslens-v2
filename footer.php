<div id="footer" class="col-lg-12 col-sm-12 text-center" style="background-color:#3c3c38; color:#fff; padding-top:10px;" > 
        <!--Social like button-->
<div>
<?php require('inc/social_media_for_website.php'); ?>
</div>

<ul class="list-inline">
        <li>
          <a href="https://www.facebook.com/TheNewsLens" target="_blank" title="The News Lens Facebook 粉絲團"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v2/assets/img/facebook.png" alt="facebook" width="32" height="32"></a>
        </li>
        <li>
          <a href="https://twitter.com/TheNewsLens" target="_blank" title="The News Lens twitter"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v2/assets/img/twitter.png" alt="twitter" width="32" height="32"></a>
        </li>
        <li>
         <a href="https://plus.google.com/+TheNewsLens/posts" target="_blank" title="The News Lens g+ 專頁"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v2/assets/img/google.png" alt="google+" width="32" height="32"></a>
        </li>
        <li>
          <a href="http://www.plurk.com/TheNewsLens" target="_blank" title="The News Lens 噗浪"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v2/assets/img/plurk.png" alt="plurk" width="30" height="30"></a>
        </li>
        <li>
          <a href="http://weibo.com/3543675662/profile?topnav=1&wvr=5&user=1" target="_blank" title="The News Lens 新浪微博"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v2/assets/img/weibo.png" alt="weibo" width="29" height="29"></a>
        </li> 
      </ul>

<ul class="list-inline">
        <li>
          <a href="http://www.thenewslens.com/news-lens%E9%97%9C%E9%8D%B5%E8%A9%95%E8%AB%96%E7%B6%B2%E5%BE%B5%E4%BA%BA%E5%95%9F%E4%BA%8B/">徵人</a>
        </li>
        <li>
          <a href="http://www.thenewslens.com/aboutus/">關於我們</a>
        </li>
        <li>
          <a href="http://www.thenewslens.com/authors/">作者介紹</a>
        </li>
        <li>
         <a href="http://www.thenewslens.com/%e5%9f%b7%e8%a1%8c%e9%95%b7%e7%9a%84%e8%a9%b1/">執行長的話</a>
        </li>
        <li>
         <a href="http://www.thenewslens.com/%e7%b8%bd%e7%b7%a8%e8%bc%af%e7%9a%84%e8%a9%b1/">總編輯的話</a>
        </li>
        <li>
          <a href="http://www.thenewslens.com/contactus/">聯絡我們</a>
        </li>
        <li>
          <a href="http://www.thenewslens.com/privacy/">隱私權聲明</a>
        </li> 
      </ul>

<p style="margin-top:5px;">Copyright &copy; <?php echo date('Y'); ?> The News Lens 關鍵評論網</p>
</div>
    <?php wp_footer(); ?>
<?php require('inc/social_media_api_footer.php'); ?>
<?php require('modal_subscription.php'); ?>
<?php require('inc/marketing_footer.php'); ?>
<?php require('plugin/full_bg.php'); ?>
<?php require('plugin/flyout_post_nav.php'); ?>

<script type="text/javascript" src="http://ad.sitemaji.com/ysm_thenewslen.js"></script>
<IFRAME STYLE="border:0px;width:0px;height:0px;" SRC="http://ad.yieldmanager.com/pixel?id= 959528&t=2"></IFRAME>
  </body>
</html>