<!DOCTYPE html>
<!--
_/\/\/\/\/\/\__/\/\________________________/\/\____/\/\______________________________________________/\/\___________________________________________
_____/\/\______/\/\__________/\/\/\________/\/\/\__/\/\____/\/\/\____/\/\______/\/\____/\/\/\/\______/\/\__________/\/\/\____/\/\/\/\______/\/\/\/\_
_____/\/\______/\/\/\/\____/\/\/\/\/\______/\/\/\/\/\/\__/\/\/\/\/\__/\/\__/\__/\/\__/\/\/\/\________/\/\________/\/\/\/\/\__/\/\__/\/\__/\/\/\/\___
_____/\/\______/\/\__/\/\__/\/\____________/\/\__/\/\/\__/\/\________/\/\/\/\/\/\/\________/\/\______/\/\________/\/\________/\/\__/\/\________/\/\_
_____/\/\______/\/\__/\/\____/\/\/\/\______/\/\____/\/\____/\/\/\/\____/\/\__/\/\____/\/\/\/\________/\/\/\/\/\____/\/\/\/\__/\/\__/\/\__/\/\/\/\___
____________________________________________________________________________________________________________________________________________________
-->
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<script type="text/javascript">if (self != top) top.location.href = self.location.href;</script>
<?php //require_once( get_template_directory() . '/inc/ga_exp.php' ); ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
    <link type="application/rss+xml" rel="alternate" title="訂閱 The News Lens 關鍵評論網" href="http://feeds.feedburner.com/TheNewsLens" />
<?php require_once( get_template_directory() . '/inc/marketing_header.php' ); ?>
<?php if(is_single()): ?>
<?php
    $post = get_post();
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');

?>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Article",
  "name" : "<?=$post->post_title?>",
  "author" : {
    "@type" : "Person",
    "name" : "<?=get_the_author();?>"
  },
  "datePublished" : "<?=$post->post_date?>",
  "image" : "<?= $image_url[0]?>",
  "articleBody" : "<?=preg_replace('/^\s+|\n|\r|\s+$/m', ' ', summarize_long_text($post->post_content, -1))?>",
  "url" : "<?=get_permalink($post->ID)?>",
  "publisher" : {
    "@type" : "Organization",
    "name" : "The News Lens"
  }
}
</script>
<?php endif; ?>
<?php require_once( get_template_directory() . '/inc/dfp_head.php' ); ?>
<?php require_once( get_template_directory() . '/inc/onead.php' ); ?>

</head>

<body class="body">
<?php require_once( get_template_directory() . '/inc/alexa_cert.php' ); ?>

<?php require_once( get_template_directory() . '/inc/google_analytics.php' ); ?>

<?php require('inc/social_media_api_header.php'); ?>

    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="" style="margin: 0 auto 0 auto; max-width:1440px;">
            <div class="navbar-header">
                <button class="navbar-toggle collapsed pull-left" type="button" data-toggle="collapse" data-target=".navbar-collapse" style="">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img width="110" class="pull-left" src="<?php echo get_template_directory_uri(); ?>/assets/img/home@2x.png"  style="padding-top:11px;"></a>
                <div class="pull-right">
                    <div class="dropdown pull-right" style="padding:5px;">
                        <a href="#" style="color:white; padding:5px;" class="dropdown-toggle fill-div" data-toggle="dropdown"><i class="glyphicon glyphicon-chevron-down pull-right" style="padding: 9px 2px 5px 5px; font-size:8px;"></i><?php echo menu_user_icon(); ?></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"></a></li>
                            <li><a href="javascript:;"><span id="btnLang" title="語系切換">语系：切换为简体</span></a></li>
                            <li class="divider"></li>
                            <li><a data-toggle="modal" href="#email-modal">訂閱免費電子報</a></li>
                            <li class="divider"></li>
                            <div style="margin-left: 15px;">
                                <?php 
                                    $user_loggedin = menu_login_logout();
                                    echo $user_loggedin;
                                ?>
                            </div>
                        </ul>
                    </div>
                    <div id="touch-click" class="dropdown pull-right" style="padding:15px 15px 15px 0;">
                        <a href="#" style="color:white;"class="dropdown-toggle fill-div" data-toggle="dropdown" ><i class="glyphicon glyphicon-search"></i></a>
                        <div class="dropdown-menu" style="padding:12px;">
                            <form class="form-inline" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) . 'search/'; ?>">
                                <button type="submit" class="btn btn-default pull-right"><i class="glyphicon glyphicon-search"></i></button>
                                <input name="query" type="text" style="width:177px;" class="form-control pull-left" placeholder="Search">
                            </form>
                        </div>
                    </div>
                </div>

                </div>
                <nav class="navbar-collapse navbar-collapse collapse pull-left " role="navigation" style="height: 1px;">
                    <div class="clearfix">
                        <ul class="nav navbar-nav mobile-nav">
                            <?php /*
                            <li><a href="category2.html">主編推薦</a></li>
                            <li><a href="category2.html">熱門文章</a></li>
                            */ ?>
	
                            <li><a href="/topic/world/">國際</a></li>
                            <li><a href="/topic/business/">財經</a></li>
                            <li><a href="/topic/politics/">政治</a></li>
                            <li><a href="/topic/society/">社會</a></li>
                            <li><a href="/topic/education/">教育</a></li>
                            <li><a href="/topic/work-field/">職場</a></li>
                            <li><a href="/topic/tech/">科技</a></li>
                            <li><a href="/topic/life/">生活</a></li>
                            <li><a href="/topic/celebrity/">名人</a></li>
                            <li><a href="/topic/medical/">醫療</a></li>
                            <li><a href="/topic/digest/">網摘</a></li>
                            <li><a href="/topic/videos/">影片 <i class="glyphicon glyphicon-facetime-video"></i></a></li>
                        </ul>
                    </div>
                </nav>
                <ul class="nav navbar-nav pull-left hidden-sm">
                    <li><a href="/">首頁</a></li>
                    <li><a href="/topic/world/">國際</a></li>
                    <li><a href="/topic/business/">財經</a></li>
                    <li><a href="/topic/politics/">政治</a></li>
                    <li><a href="/topic/society/">社會</a></li>
                    <li><a href="/topic/education/">教育</a></li>
                    <li class="hidden-md"><a href="/topic/work-field/">職場</a></li>
                    <li class="hidden-md"><a href="/topic/tech/">科技</a></li>
                    <li class="hidden-md"><a href="/topic/life/">生活</a></li>
                    <li class="hidden-md"><a href="/topic/celebrity/">名人</a></li>
                    <li class="hidden-md"><a href="/topic/medical/">醫療</a></li>
                    <li class="hidden-md"><a href="/topic/digest/">網摘</a></li>
                    <li class="hidden-lg dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">其他</a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                        <li><a href="/topic/work-field/">職場</a></li>
                        <li><a tabindex="-1" href="/topic/tech/">科技</a></li>
                        <li><a tabindex="-1" href="/topic/life/">生活</a></li>
                        <li><a tabindex="-1" href="/topic/celebrity/">名人</a></li>
                        <li><a tabindex="-1" href="/topic/medical/">醫療</a></li>
                        <li><a tabindex="-1" href="/topic/digest/">網摘</a></li>
                        </ul>
                    </li>
                    <li><a href="/topic/videos/">影片 <i class="glyphicon glyphicon-facetime-video"></i></a></li>
        </ul>
        </div>
    </div>
    </header>

    <div id="top-banner" class="col-lg-12">
        <div class="hidden-sm" style="height:30px; max-width: 770px; margin:0 auto 0 auto;">
            <img width="25px" height="25px" style="vertical-align: top; display: inline-block; margin-top:4px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_blur.png" style="width:29px; ">
            <span style="padding:4px; vertical-align: top; display: inline-block;"><b>News worth knowing, voices worth sharing</b> 分享觀點從這開始  </span>
            <div style="height:22px; width: 275px; padding-top:6px; display:inline-block;">
<?php require('inc/social_media_for_website.php'); ?>
            </div>
        </div>
        <div class="visible-sm" style="height:30px; width: 275px; margin:0 auto 0 auto;">
            <div style="height:22px; width:275px; padding-top:6px; display:inline-block;">
<?php require('inc/social_media_for_website.php'); ?>
            </div>
        </div>
    </div>