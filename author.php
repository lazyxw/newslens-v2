<?php get_header(); ?>
<?php 

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
?>

    <div class="clearfix"></div>

    <div>
    <div class="col-lg-1 hidden-sm hidden-xs"></div>
    <div id="left-sidebar" class="col-lg-3">
<?php
if( is_single() ) {
    $author_panel_add_class = ' hidden-sm hidden-xs';
} else {
    $author_panel_add_class = '';
}
?>
        <div class="panel<?php echo $author_panel_add_class; ?>">
            <?php get_template_part( 'author-bio' ); ?>
        </div>
        <?php require('inc/TNL_AuthorPage_160x600.php'); ?>
    </div>
    <div id="main-content" class="col-lg-7 col-sm-12">
<?php get_latest_list(10, $paged, false, false, get_query_var('author')); ?>
    </div>

    <div id="right-top-sidebar" class="col-lg-3 col-sm-4" style="height:100%"></div>

    <div class="col-lg-12 col-md-12 col-sm-12" style="height:100px;" >
        <div class="paginator_container">
<?php get_latest_list(10, $paged, false, false, get_query_var('author'), true); ?>
        </div>
    </div>
        

    </div>

</div>


<?php get_footer(); ?>