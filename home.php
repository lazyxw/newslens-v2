<?php get_header(); ?>
<?php 

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
$latest_posts_per_page = 10;
?>

<div id="content-body">
    <div id="cat-nav">      		
    	<div id="" class="cat-nav-single col-lg-3 visible-lg" style="background-color: #000000; height:30px;max-width:360px; opacity:0.7;"><span style="color:white;">熱門文章</span></div>
        <div id="" class="cat-nav-single col-lg-6 col-sm-8" style="background-color: #000000; height:30px;max-width:720px; opacity:0.85;"><span style="color:white;">最新文章</span><a href="http://www.thenewslens.com/rsslist/"><span class="pull-right" style="color:#fff;"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v2/assets/img/rss.png" width="20px" style="padding:3px;">訂閱</span></a></div>
    	<div id="" class="cat-nav-single col-lg-3 col-sm-4 hidden-sm" style="background-color: #000000; height:30px;max-width:360px; opacity:0.7;"><span style="color:white;">編輯精選</span></div>
	</div>

    <div id="content">
    	<div id="left-sidebar" class="col-lg-3 visible-lg">
        <?php get_popular_list_with_GA(23); ?>
    	</div>

    	<div id="main-content" class="col-lg-6 col-sm-8">
        <?php get_latest_list($latest_posts_per_page, $paged); ?>
    	</div>

    	<div id="right-top-sidebar" class="col-lg-3 col-sm-4 hidden-xs hidden-sm">
        <?php // require_once('inc/highlight_tag.php'); ?>
        <?php get_recent_comments(); ?>
        <?php get_mario_digest_short(); ?>
        <?php get_featured_list(7); ?>
        </div>
        
        <div class="col-lg-12 col-md-12 col-sm-12" style="height:100px;" >
            <div class="paginator_container">
                <?php get_latest_list($latest_posts_per_page, 1, false, false, false, true); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>