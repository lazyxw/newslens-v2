<?php
/*
    Template Name: rss list
*/
?>
<?php get_header(); ?>

    <div id="left-sidebar" class="col-lg-2 visible-lg">
            
    </div>

    <div id="main-content" class="panel col-lg-8 col-sm-8" style="padding:20px">
        <h3><img src="<?php echo get_template_directory_uri(); ?>/assets/img/tnl-rss.png" style="width:60px;height:60px;" title="訂閱 The News Lens 關鍵評論網 RSS"> RSS 訂閱</h3>

        <div class="author-list">
            <div class="author-wrap panel">
                <div class="desc">
                    <h4>全部文章</h4>
                    訂閱 The News Lens 關鍵評論網提供的所有文章。
                    <br>
                    <a href="http://feeds.feedburner.com/TheNewsLens" target="_blank">http://feeds.feedburner.com/TheNewsLens</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="author-list">
            <div class="author-wrap panel">
                <div class="desc">
                    <h4>新聞</h4>
                    訂閱 The News Lens 關鍵評論網提供的所有新聞。
                    <br>
                    <a href="http://feeds.feedburner.com/thenewslens/news" target="_blank">http://feeds.feedburner.com/thenewslens/news</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="author-list">
            <div class="author-wrap panel">
                <div class="desc">
                    <h4>評論</h4>
                    訂閱 The News Lens 關鍵評論網提供的所有評論。
                    <br>
                    <a href="http://feeds.feedburner.com/thenewslens/review" target="_blank">http://feeds.feedburner.com/thenewslens/review</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="author-list">
            <div class="author-wrap panel">
                <div class="desc">
                    <h4>影片</h4>
                    訂閱 The News Lens 關鍵評論網提供的所有影片。
                    <br>
                    <a href="http://feeds.feedburner.com/thenewslens/video" target="_blank">http://feeds.feedburner.com/thenewslens/video</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>

    <div id="right-top-sidebar" class="col-lg-2 col-sm-4 hidden-xs hidden-sm" style=""></div>

<?php get_footer(); ?>