  <div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="email-modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/header-banner.png" style="width: 80%; height: auto; padding:10px;">
        </div>
        <div class="modal-body">
          親愛的讀者，          <br>

          歡迎登入我們的網站，我們目前只提共 Facebook 登入, 來紀錄您閱讀過的文章，以及訂閱我們的電子報的功能。          <br>

          感謝您的支持。
          <br>
          -------------------------------------------
          <br>
          <div class="newsletter newsletter-subscription">
  <!-- <form method="post" action="http://www.thenewslens.com" onsubmit="return newsletter_check(this)">
    <input type="hidden" name="na" value="s">
    <table>
      <tbody>
        <tr>
          <th>Email</th>
          <td align="left">
            <input class="newsletter-email" type="text" name="ne" size="30" value="<?php menu_user_email(); ?>"></td></tr><tr><td colspan="2" style="text-align: center">
            <input type="submit" value="訂閱">
          </td>
        </tr>
      </tbody>
    </table>
  </form> -->


<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="http://thenewslens.us3.list-manage.com/subscribe/post?u=a0d4f386a600c0574144f8895&amp;id=61a0b84fff" onsubmit="return newsletter_check(this)" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	<label for="mce-EMAIL">訂閱關鍵評論網的電子報</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="請在這裡輸入您的電子郵件信箱" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_a0d4f386a600c0574144f8895_61a0b84fff" value=""></div>
	<div class="clear"><input type="submit" value="訂閱" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</form>
</div>

<!--End mc_embed_signup-->



</div>

        </div>
        <div class="modal-footer">
            <div style="width:120px; margin:0 auto 0 auto;">
              <!--<a rel="nofollow" href="http://www.thenewslens.com/wp-login.php?action=wordpress_social_authenticate&amp;provider=Facebook&amp;redirect_to=http%3A%2F%2Fwww.thenewslens.com%2Fwp-login.php" title="Connect with Facebook" class="wsl_connect_with_provider">
                <button type="button" class="btn btn-large btn-primary">facebook 登入</button>
              </a> -->
              <?php 
                    $user_loggedin = menu_login_logout();
                    echo $user_loggedin;
              ?>
            </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
