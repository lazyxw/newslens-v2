<?php get_header(); ?>
<?php 

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
$cate_id = get_query_var('cat');
$cat_name = get_category($cate_id)->name;

$mario_cate_name = '馬力歐報報';
?>
<div id="content-body">
    <div id="cat-nav" class="affix-top">            
        <div id="" class="col-lg-1  visible-lg" style="height:30px;max-width:360px;"></div>
        <div id="" class="col-lg-7 col-sm-8" style="background-color:#373A38; height:30px;max-width:841px; color:white;">分類: <?php echo $cat_name; ?><span class="pull-right" style="color:white;">  最新文章</span></div>
        <div id="" class="col-lg-3 col-sm-4 hidden-sm" style="background-color:#888; height:30px; max-width:360px;"><span style="color:white;">熱門文章</span></div>
        
    </div>
    <div class="clearfix"></div>
    <div>
        <div id="left-sidebar" class="col-lg-1 hidden-sm hidden-xs"></div>
        <div id="main-content" class="col-lg-7 col-sm-8">
            <?php
                if ($cat_name == $mario_cate_name){
                    get_mario_digest(10, $paged);
                } else {
                    get_latest_list(10, $paged, false, $cate_id, false); 
                }
            ?>
        </div>

        <div id="right-top-sidebar" class="col-lg-3 col-sm-4 hidden-sm hidden-xs" style="height:100%">
            <?php // require_once('inc/highlight_tag.php'); ?>
            <?php
                if ($cat_name == $mario_cate_name){
                    get_popular_list(5, 1, false, false, false, false, 14);
                } else {
                    get_popular_list(10, 1, false, $cate_id, false, false, 14); 
                }
            ?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:100px;" >
            <div class="paginator_container">
            <?php
                if ($cat_name == $mario_cate_name){
                    get_mario_digest(10, $paged, true);
                } else {
                    get_latest_list(10, $paged, false, $cate_id, false, true);
                }
            ?>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
