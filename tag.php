<?php get_header(); ?>
<?php 

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
$cur_tag = get_query_var('tag');
$cur_tag_obj = get_term_by('slug', $cur_tag, 'post_tag');
$tag_name = strtoupper($cur_tag_obj->name);
?>
    <div id="cat-nav" class="affix-top">            
        <div id="" class="col-lg-1  visible-lg" style="background-color:rgba(228, 228, 228, 0); height:30px;max-width:360px;"></div>
        <div id="" class="col-lg-7 col-sm-8" style="background-color:#373A38; height:30px;max-width:841px; color:white;">標籤: <?php echo $tag_name; ?><span class="pull-right" style="color:white;">  最新文章</span></div>
        <div id="" class="col-lg-3 col-sm-4 hidden-sm" style="background-color:#888; height:30px; max-width:360px;"><span style="color:white;">熱門文章</span></div>
        
    </div>

    <div class="clearfix"></div>

    <div>
    <div id="left-sidebar" class="col-lg-1 hidden-sm hidden-xs"></div>
    <div id="main-content" class="col-lg-7 col-sm-8">
<?php get_latest_list(10, $paged, $cur_tag, false,false); ?>
    </div>

    <div id="right-top-sidebar" class="col-lg-3 col-sm-4 hidden-sm hidden-xs" style="height:100%">
        <?php // require_once('inc/highlight_tag.php'); ?>
<?php get_popular_list(10, $paged = 1, $cur_tag, false, false, false, 30); ?>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:100px;" >
        <div class="paginator_container">
<?php get_latest_list(10, $paged, $cur_tag, false, false, true); ?>
        </div>
    </div>

        


<?php get_footer(); ?>